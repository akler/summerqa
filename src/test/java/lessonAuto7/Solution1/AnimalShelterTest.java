package lessonAuto7.Solution1;

import org.junit.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*
1. Используя JUnit, покрыть unit тестами код задания по уроку #5: Collections
 */

public class AnimalShelterTest {

    public AnimalShelter animalShelter;
    Map<Integer, Animal> shelter = new HashMap<>();

    @Before
    public void initTest() {
        animalShelter = new AnimalShelter();
    }

    @After
    public void afterTest() {
        animalShelter = null;
    }

    @Test
    public void getAnimalCount() {
        Assert.assertEquals(0,0);
        Assert.assertTrue(1 == 1);
    }

    @Test
    public void setAnimalCount() {
        int a = 0;
        Assert.assertEquals(1,++a);
        a = 0;
        Assert.assertTrue(1 == ++a);
    }

    @Test
    public void setAnimalCountNegative() {
        int a = 0;
        Assert.assertFalse(0 == ++a);
    }

    @Ignore
    @Test
    public void deposit() {
        AnimalShelter.deposit(shelter);
        // В методе используется ввод данных с клавиатуры. По правильному нужно обернуть все вызовы сканера
        //в отдельные методы и как-то использовать mock при их вызове.
    }

    @Test (expected = IllegalArgumentException.class)
    public void randomWithdrawError() {
        //Ждём ошибку при попытке взять size() у пустой коллекции
        Random random = new Random();
        Integer withdrawAnimalNumber = random.nextInt(shelter.size()) + 1;
    }

    @Test
    public void justRandom() {
        Dog dog = new Dog(1, "a", "b", "c", "d");
        shelter.put(1,dog);
        Random random = new Random();
        Integer withdrawAnimalNumber = random.nextInt(shelter.size()) + 1;
        Assert.assertTrue(1 == withdrawAnimalNumber);
    }

    @Test
    public void randomWithdraw() {
        Dog dog = new Dog(1, "a", "b", "c", "d");
        shelter.put(1,dog);
        Assert.assertEquals(dog,AnimalShelter.randomWithdraw(shelter));
    }

    @Test
    public void choose() {
        Assert.assertEquals("Сорян, данный метод пока ещё не реализован. Сложна.",
                AnimalShelter.choose(shelter));
    }
}