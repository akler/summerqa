package lessonAuto10;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static lessonAuto10.Driver.getWebDriver;
import static org.hamcrest.MatcherAssert.assertThat;

public class GoogleSearchTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = getWebDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void shouldTestSearchResults() {
        String searchText = "alfa-bank";
        GoogleMainPage googleMainPage = new GoogleMainPage(driver);
        googleMainPage.open();
        googleMainPage.searchForText(searchText);
        GoogleSearchResultPage googleSearchResultPage = new GoogleSearchResultPage(driver);
        List<String> actualSearchResultNames = googleSearchResultPage.getSearchResultNames();

//        assertThat(actualSearchResultNames.size(), not(0));
//        assertThat(actualSearchResultNames.toString(), actualSearchResultNames
//         .stream()
//         .allMatch(item->item.contains(searchText)), is(true));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
