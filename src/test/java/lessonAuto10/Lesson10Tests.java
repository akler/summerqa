package lessonAuto10;

import lessonAuto10.Driver;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

import static lessonAuto10.Driver.getWebDriver;

public class Lesson10Tests {
    WebDriver driver;

    final static String URL = "https://store.nike.com/ru/ru_ru/";
    final static String FIRST_ITEM = "(//div[@class='grid-item-image'])[1]";

    @Before
    public void setUp() {
        driver = getWebDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(URL);
    }

    @Test
    public void firstElementHoverTest() {
        WebElement element = driver.findElement(By.xpath(FIRST_ITEM));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).build().perform();

    }

    @After
    public void killDriver() {
        driver.quit();
    }
}
