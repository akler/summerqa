package lessonAuto10;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static lessonAuto10.Driver.getWebDriver;

public class GoogleMainPage {
    WebDriver driver;

    public GoogleMainPage (WebDriver driver) {
        this.driver = driver;
    }

    final static String URL = "http://www.google.com";
    final static String INPUT = "//input[@id='lst-ib']";

    public void open() {
        driver.get(URL);
    }

    public void searchForText(String text) {
        WebElement inputField = driver.findElement(By.xpath(INPUT));
        inputField.sendKeys();
        inputField.sendKeys(Keys.ENTER);
    }
}
