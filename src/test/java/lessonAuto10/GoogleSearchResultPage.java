package lessonAuto10;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

import static lessonAuto10.Driver.getWebDriver;

public class GoogleSearchResultPage {
    WebDriver driver;

    public GoogleSearchResultPage (WebDriver driver) {
        this.driver = driver;
    }

    final static String RESULT = ".rc .r";


    public List<String> getSearchResultNames() {
        List<WebElement> getSearchResultNames = driver.findElements(By.cssSelector(RESULT));
        return getSearchResultNames
                .stream()
                .map(webElement -> webElement.getText().toLowerCase().trim())
                .collect(Collectors.toList());
    }
}
