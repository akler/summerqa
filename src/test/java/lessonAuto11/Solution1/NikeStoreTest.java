package lessonAuto11.Solution1;

/*
Задание по уроку #5: Selenide
Выполнить ДЗ из занятия 4 (Selenium webdriver extended) полностью используя Selenide:
• Проверить описание товара после его выбора из списка всех товаров;
• Проверить, что товар добавляется в корзину.
 */

import com.codeborne.selenide.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

public class NikeStoreTest {

    NikeStorePage nikeStorePage = new NikeStorePage();
    NikeProductDetailsPage nikeProductDetailsPage = new NikeProductDetailsPage();
    NikeCartPopup nikeCartPopup = new NikeCartPopup();
    NikeCartPage nikeCartPage = new NikeCartPage();
    String typeXpath = "//a/span[contains(text(),'МУЖЧИНЫ')]";
    String productCss = "a[data-subnav-label=\"Вся обувь\"]";

    @Before
    public void setUp() {
        Configuration.baseUrl = "http://store.nike.com";
    }

    @Test
    public void testMenShoesBuying() {
        nikeStorePage.open();
        nikeStorePage.chooseProductCategory(typeXpath, productCss);
        WebElement product = nikeStorePage.randomProductChoose();
        WebElement modelFromStore = nikeStorePage.findModel();
        WebElement priceFromStore = nikeStorePage.findPrice();
        product.click();
        nikeProductDetailsPage.selectSize();
        nikeProductDetailsPage.addToCart();
        nikeCartPopup.goToCart();
        assert nikeCartPage
                .findModel().getText()
                .contains(modelFromStore.getText());
        assert nikeCartPage
                .findPrice().getText()
                .equals(priceFromStore.getText());
    }

    @Test
    public void testMenShoesDetailsPageInfo() {
        nikeStorePage.open();
        nikeStorePage.chooseProductCategory(typeXpath, productCss);
        WebElement product = nikeStorePage.randomProductChoose();
        String modelFromStore = nikeStorePage.findModel().getText();
        String priceFromStore = nikeStorePage.findPrice().getText();
        product.click();
        assert nikeProductDetailsPage
                .findPrice().getText().equals(priceFromStore);
        assert nikeProductDetailsPage
                .findModel().getText().equals(modelFromStore);

    }
}