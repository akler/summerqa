package lessonAuto11;

import com.codeborne.selenide.Configuration;
import org.junit.Before;
import org.junit.Test;

public class Lesson11GoogleExampleTest {
    GoogleSearchResultPage googleSearchResultPage;

    @Before
    public void setUp() {
        Configuration.baseUrl = "https://www.google.com/";
    }

    @Test
    public void searchGoogleResultTest() {
        String searchText = "альфа-банк";
        GoogleMainPage googleMainPage = new GoogleMainPage();
        googleMainPage.openPage();
        googleSearchResultPage = googleMainPage.searchForText(searchText);
        googleSearchResultPage
                .resultsQuantityShouldBe(6)
                .resultsTextShouldContain(searchText);

    }
}
