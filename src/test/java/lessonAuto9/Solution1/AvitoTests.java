package lessonAuto9.Solution1;

import org.openqa.selenium.WebDriver;

/*
Задание по уроку #3: Локаторы
https://www.avito.ru/moskva/transport

Используя xpath (css) локаторы найти:
Названия всех мотоциклов
Названия автомобилей, объявления о продаже которых были добавлены сегодня
Первую Audi*(любую марку)
 */

public class AvitoTests {

//  final static WebDriver driver = Driver.getFirefoxDriver();

    final static String URL = "https://www.avito.ru/moskva/transport";
    static String allMotoXpath = "//a[contains(@href,'mototsikly_i_mototehnoka')]/span[contains(text(),'')]";
    static String allMotoCss = "a[href*='mototsikly_i_mototehnika'] span[itemprop=name]";
    static String autoTodayXpath = "//div[contains(text(),'Сегодня')]//../..//a[contains(@href,'avtomobili')]/span[contains(text(),'')]";
    static String autoTodayCss = "";
    static String firstAutoXpath = "(//a[contains(@href,'avtomobili')]/span[contains(text(),'Mercedes-Benz')])[1]";
    static String firstAutoCss = "";
}
