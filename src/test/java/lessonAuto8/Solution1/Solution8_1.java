package lessonAuto8.Solution1;

import lessonAuto8.Driver;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
Написать автоматизированные тестовые сценарии для формы регистрации на странице:
https://www.survio.com/

Как минимум один позитивный сценарий
Как минимум один негативный сценарий

 */
public class Solution8_1 {
    final static WebDriver driver = Driver.getWebDriver();

    final static String URL = "https://www.survio.com/";
    final static String NAME = "[name=reg_name]";             //css по атрибут="значение", где атрибут это имя элемента
    final static String EMAIL = "#reg_email";                 //css по id
    final static String PASSWORD = "[type=password]";         //css по атрибут="значение", где атрибут это тип элемента
    final static String BUTTON = "a.create-survey";           //css по классу в элементе а
    final static String ERROR_MESSAGE = "#reg_email-error";   //css по id
    final static String WELCOME_MESSAGE = "//div[@id=\"menu\"]//div[@class=\"in\"]//h1"; //xpath (id=menu/class=in/h1)


    final static String INPUT_NAME = "Даниил Валесски";
    final static String INPUTE_MAIL = "test4@test.ua";
    final static String INPUTE_PASSWORD = "abrakadabra";

    @Test
    public void testRegistrationWithEmptyFields() {
        driver.get(URL);
        WebElement inputName = driver.findElement(By.cssSelector(NAME));
        inputName.clear();
        inputName.sendKeys(INPUT_NAME);

        WebElement inputPassword = driver.findElement(By.cssSelector(PASSWORD));
        inputPassword.clear();
        inputPassword.sendKeys(INPUTE_PASSWORD);

        WebElement button = driver.findElement(By.cssSelector(BUTTON));
        button.click();

        WebElement errorMessage = driver.findElement(By.cssSelector(ERROR_MESSAGE));
        String result = errorMessage.getText();

        String currentUrl = driver.getCurrentUrl();
        if (currentUrl.endsWith("/ru/")) {
            assert result.equals("Это поле необходимо заполнить");
        }
        else if (currentUrl.endsWith("/en/")) {
            assert result.equals("Please enter a valid email address.");
        }
        else {
            assert false;
        }
    }

    @Test
    public void testRegistration() {
        driver.get(URL);
        WebElement inputName = driver.findElement(By.cssSelector(NAME));
        inputName.clear();
        inputName.sendKeys(INPUT_NAME);

        WebElement inputEmail = driver.findElement(By.cssSelector(EMAIL));
        inputEmail.click();
        inputEmail.clear();
        inputEmail.sendKeys(INPUTE_MAIL);

        WebElement inputPassword = driver.findElement(By.cssSelector(PASSWORD));
        inputPassword.clear();
        inputPassword.sendKeys(INPUTE_PASSWORD);

        String currentUrl = driver.getCurrentUrl();

        WebElement button = driver.findElement(By.cssSelector(BUTTON));
        button.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement welcomeMessage = wait.until( d-> {
            return d.findElement(By.xpath(WELCOME_MESSAGE));
        });

        String result = welcomeMessage.getText();
        if (currentUrl.endsWith("/ru/")) {
            assert result.equals("Каким способом Вы хотите создать опрос?");
        }
        else if (currentUrl.endsWith("/en/")) {
            assert result.equals("How do You Want to Create Your Survey?");
        }
        else {
            assert false;
        }

    }

    @AfterClass
    public static void killDriver() {
        driver.close();
    }
}
