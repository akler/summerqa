package lessonJava1.Solution4;

public class Solution1_4 {
    /*
Задача 4. В переменной n хранится целое двузначное число. Создайте программу, вычисляющую и выводящую на экран сумму
всех цифр числа n.
Например для числа 10, сумма его цифр будет 1+0=1

     */
    public static void main(String[] args) {
        int n = 42;
        int a = n / 10;
        int b = n % 10;
        int x = a + b;
        System.out.println(x);
    }
}