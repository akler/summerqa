package lessonJava1.Solution3;

public class Solution1_3 {
    /*
public class Solution3 {

   public static void main(String[] args) {
       double c = 2.0;
       byte n = 50;
       long a = 200
       int result = (a/n) * c;
   }
}

Исправьте программу, чтобы она компилировалась.

     */
    public static void main(String[] args) {
        double c = 2.0;
        byte n = 50;
        long a = 200;

        //1й вариант (с дробной частью)
        double result = (a/n) * c;
        System.out.println(result);

        //2й вариант (с целочисленной частью)
        long result2 = (a/n) * (int) c;
        System.out.println(result2);
    }
}

