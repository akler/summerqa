package lessonJava1.Solution1;

public class Solution1_1 {
    /*
    Задача 1. В переменных n и j хранятся два целых числа. Создайте программу, выводящую на экран результат деления n
    на j с остатком.
Пример вывода программы (для случая, когда в n хранится 21, а в j  хранится 8):
                    21 / 8 = 2 и 5 в остатке
     */
    public static void main(String[] args) {
        int n = 189;
        int j = 12;
        int a = n / j;
        int b = n % j;
        System.out.println("Результат деления: " + a + " и " + b + " в остатке");
    }
}