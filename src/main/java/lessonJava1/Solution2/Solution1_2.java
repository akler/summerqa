package lessonJava1.Solution2;

public class Solution1_2 {
    /*
    Задача 2. В переменной  double n хранится число  с ненулевой дробной частью.
    Создайте программу, округляет число n до ближайшего целого и выводящую результат на экран.
     */
    public static void main(String[] args) {
        double n = 12.3;
        double e = 0.5;
        int a = (int) n;
        double b = n - a;
        if (b >= e)
        {
            ++a;
        }
        System.out.println(a);
    }
}