package lessonJava3.Solution3;

public abstract class Creature implements Brawl {
    public double health;
    public double strength;

    Creature() {
    }

    public double getHealth() {
        return health;
    }

    public double getStrength() {
        return strength;
    }

    @Override
    public void fighting(Creature opponent) {
    }
}
