package lessonJava3.Solution3;

class Animal extends Creature {
    public String gender;
    public String name;
    double health;
    double strength;

    Animal() {
    }

    Animal(String name) {
        this.name = name;
    }

    Animal(String gender, String name) {
        this.gender = gender;
        this.name = name;
    }

    @Override
    public double getHealth() {
        return health;
    }

    @Override
    public double getStrength() {
        return strength;
    }
    
    public void fighting(Animal opponent) {
        if (this.equals(opponent)) {
            System.out.println(this.name + ", драться самому с собой это не спортивно.");
        }
        else if (this.getClass().equals(opponent.getClass())) {
            System.out.println("Пчеловоды не должны сражаться друг с другом.");
        }
        else {
            double animalHealth = this.getHealth();
            double opponentHealth = opponent.getHealth();
            String winner;

            System.out.println(this.name + " ввязывается в драку c " + opponent.name);
            System.out.printf("Бой не равный: у %s %.1f жизней и сила удара %.1f. У %s тем временем %.1f жизней, но сила удара %.1f",
                    this.name, animalHealth, this.getStrength(), opponent.name, opponentHealth, opponent.getStrength());
            System.out.println();
            do {
                double animalAttack = Math.random();
                double opponentAttack = Math.random();
                if (animalAttack > opponentAttack) {
                    opponentHealth -= damage * this.getStrength();
                    System.out.println("Вот это удар! Ещё чуть-чуть чуть и " + opponent.name + " получит по заслугам.");
                    System.out.printf("У %s %.1f жизней, у %s осталось %.1f", this.name, animalHealth, opponent.name, opponentHealth);
                    System.out.println();
                    System.out.println();
                } else if (animalAttack < opponentAttack) {
                    animalHealth -= damage * opponent.getStrength();
                    System.out.println(opponent.name + " бьёт противника по сусалам.");
                    System.out.printf("У %s %.1f жизней, у %s осталось %.1f", this.name, animalHealth, opponent.name, opponentHealth);
                    System.out.println();
                    System.out.println();
                } else {
                    System.out.println("Силы их были равны и в этом раунде удивительная ничья.");
                }
            }
            while (animalHealth > 0 && opponentHealth > 0);

            if (animalHealth <= 0) {
                winner = opponent.name;
            } else if (opponentHealth <= 0) {
                winner = this.name;
            } else {
                winner = "Удивительное дело, но победила дружба. То есть никто.";
            }

            System.out.printf("Победитель раунда: %s!", winner);
            System.out.println();
        }
    }

    public static void run(){
        System.out.println("Я бегу как животное");
    }

    public static void eat(){
        System.out.println("Омном-ном!");
    }

    public static void bark(){
        System.out.println("Голос!");
    }
}
