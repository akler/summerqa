package lessonJava3.Solution3;

public class Solution3_3 {
    /*
Задача 3. Теперь все существа (животные и не только) умеют драться между собой. На основании кода из прошлых заданий,
реализуйте механизмы межвидовой борьбы.
(Подсказка. Нужно использовать абстрактные классы и интерфейсы).
     */
    public static void main(String[] args) {

        Cat nissa = new Cat("Кошка", "Нисса", "Тенистый проезд", "Вова");
        Cat dhus = new Cat("Кот", "Дхуся", "Серпухов", "Ари");
        Dog seymour = new Dog("Сэймур",null);
        Dog sparky = new Dog(null, "3-я улица Строителей, кв.12");

        System.out.println("Начинается межвидовая борьба за равенство и братство");
        System.out.println();
        nissa.fighting(dhus);
        System.out.println("=====");
        dhus.fighting(dhus);
        System.out.println("=====");
        dhus.fighting(seymour);
        System.out.println("=====");
        sparky.fighting(seymour);
    }
}
