package lessonJava3.Solution2;

public class Cat extends Animal implements Brawl {
    String homePlace;
    String master;

    Cat(String gender, String name, String homePlace, String master) {
        super(gender, name);
        this.homePlace = homePlace;
        this.master = master;
    }

    public static void bark() {
        System.out.println("Миииияу!");
    }

    @Override
    public void fighting(Cat opponent) {
        if (this.equals(opponent)) {
            System.out.println(this.name + ", драться самому с собой это не спортивно.");
        }
        else {

            int catHealth = 9;
            int opponentHealth = 9;
            String winner;

            System.out.println(this.name + " ввязывается в драку c " + opponent.name);
            do {
                double catAttack = Math.random();
                double opponentAttack = Math.random();
                if (catAttack > opponentAttack) {
                    opponentHealth -= damage;
                    System.out.println("Вот это удар! Ещё чуть-чуть чуть и " + opponent.name + " останется без хвоста.");
                    System.out.printf("У %s %d жизней, у %s осталось %d", this.name, catHealth, opponent.name, opponentHealth);
                    System.out.println();
                    System.out.println();
                } else if (catAttack < opponentAttack) {
                    catHealth -= damage;
                    System.out.println(opponent.name + " кусает прямо за ус! Хитро.");
                    System.out.printf("У %s %d жизней, у %s осталось %d", this.name, catHealth, opponent.name, opponentHealth);
                    System.out.println();
                    System.out.println();
                } else {
                    System.out.println("Силы их были равны и в этом раунде удивительная ничья.");
                }
            }
            while (catHealth > 0 && opponentHealth > 0);

            if (catHealth == 0) {
                winner = opponent.name;
            } else if (opponentHealth == 0) {
                winner = this.name;
            } else {
                winner = "Удивительное дело, но победила дружба. То есть никто.";
            }

            System.out.printf("Победитель раунда: %s!", winner);
            System.out.println();
        }
    }
}
