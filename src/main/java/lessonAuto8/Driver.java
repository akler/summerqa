package lessonAuto8;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Driver {
    private static WebDriver driver;

    private Driver () {}

    public static WebDriver getWebDriver() {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
        return driver;
    }

    public void killDriver() {
        driver.close();
    }
}
