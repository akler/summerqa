package lessonJava2;

public class lesson2_if_examples {
    public static void main(String[] args)
    {
        int a = 11;
        if (a==10){
            System.out.println("a eq 10");
        }else if(a==11)
        {
            System.out.println("a eq 11");
        }else{
            System.out.println("a not 10");
        }
        System.out.println("=========");

        int x = 10;
        int y = 1;
        do {
            y++;
            if (0 == y % 2)
            System.out.println(y);
        }
        while (y<=x);
    }
}
