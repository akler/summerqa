package lessonJava2.Solution3;

public class Solution2_3 {
    /*
Задача 3. Создать программу, которая будет проверять попало ли случайно выбранное из отрезка [5;155] целое число
в интервал (25;100) и сообщать результат на экран.
Примеры работы программы:
Число 113 не содержится в интервале (25,100)
Число 72 содержится в интервале (25,100)
 */
    public static void main(String[] args){

        int min = 5;
        // max равен 150, так как в любом случае прибавляем начальные 5 к диапазону
        int max = 150;
        int from = 25;
        int to = 100;
        String ANSI_YELLOW = "\u001B[33m";
        String ANSI_RED = "\u001B[31m";

        // Минимум 5+0.0*150=5, максимум 5+1.0*150
        int randomNumber = min + (int) (Math.random() * max);

        if (randomNumber > from && randomNumber < to)
            System.out.printf(ANSI_YELLOW + "Число %d содержится в интервале (%d,%d)", randomNumber, from, to);
        else
            System.out.printf(ANSI_RED + "Число %d не содержится в интервале (%d,%d)", randomNumber, from, to);
    }
}
