package lessonJava2;

public class lesson2_string_examples {
    public static void main(String[] args)
    {
        String stringWithPlaceholders = "Привет, %s! Меня зовут %s и мне %d лет";
        String name = "Вася";
        String myName = "Петя";
        int age = 99;
        String res = String.format(stringWithPlaceholders, name, myName, age);
        System.out.println(res);
        System.out.println(myName.split("е")[1]);
        System.out.println(myName.charAt(2));
        System.out.println(myName.equals("Петя"));

        // Тренируем строки
        String hello = "привет";
        String modify = hello.concat("! Как жизнь?");
        String result = modify.replace("п", "П");
        System.out.println(result);
        String upCase = result.toUpperCase();
        System.out.println(upCase.length());

        // Единая строка
        String helloMod = "привет";
        String modifyMod = hello.concat("! Как жизнь?").replace("п", "П").toUpperCase();
        System.out.println(modifyMod.length());

    }
}
