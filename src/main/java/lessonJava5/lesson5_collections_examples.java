package lessonJava5;

import java.util.*;

public class lesson5_collections_examples {

    public static void main(String[] args) {
        List<String> myList = new ArrayList<>();
        myList.add("Data");
        myList.add("Another Data");
        System.out.println(myList.size());

        List<String> mySecondList = new ArrayList<>();
        mySecondList.addAll(myList);
        mySecondList.add("Another Data for second list");
        System.out.println(mySecondList);

        Map<Integer, String> myMap = new HashMap<>();
        myMap.put(1, "Password");
        System.out.println(myMap);
        System.out.println(myMap.get(1));

        Set<String> mySet = new HashSet<>();
        mySet.add("Cat");
        mySet.add("Cat");
        mySet.add("Lizard");
        mySet.add("Dog");
        System.out.println(mySet);

        for (String element : mySet) {
            if (element.equals("Lizard"))
                System.out.println("Yaaaaay!");
        }


    }
}
