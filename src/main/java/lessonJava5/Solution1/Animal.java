package lessonJava5.Solution1;

abstract class Animal {

    String name;
    int age;
    String breed;
    String color;
    String gender;

    Animal (int age, String name, String breed, String color, String gender) {
        this.age = age;
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.gender = gender;
    }
}
