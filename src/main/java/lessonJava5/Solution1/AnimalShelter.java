package lessonJava5.Solution1;

import java.util.*;

public class AnimalShelter {

    static Integer animalCount = 0;

    public static Integer getAnimalCount() {
        return animalCount;
    }

    public static void setAnimalCount(Integer animalCount) {
        AnimalShelter.animalCount = animalCount;
    }

    public static void deposit(Map animalShelter) {
        String animalDeposit = "";
        System.out.println("Кого вы хотите сдать в приют:");
        System.out.println("кошку или собаку?");
        Scanner scanner = new Scanner(System.in);
        String opinion = scanner.nextLine();

        if (opinion.toLowerCase().equals("кошку")) {
            animalDeposit = "cat";
        }
        else if (opinion.toLowerCase().equals("собаку")) {
            animalDeposit = "dog";
        }
        else {
            System.out.println("Сорян, наш приют принимает только кошек и собак ={");
            return;
        }

        System.out.println("Как зовут питомца?");
        String name = scanner.nextLine();
        System.out.println("Что за порода?");
        String breed = scanner.nextLine();
        System.out.println("Цвет окраса?");
        String color = scanner.nextLine();
        System.out.println("Мальчик или девочка?");
        String gender = scanner.nextLine();
        System.out.println("Сколько лет?");
        Integer age = 0;
        try {
            age = scanner.nextInt();
        }
        catch (Exception e) {
            System.out.println("Вы ввели нечто не похожее на возраст");
            age = null;
        }

        if (animalDeposit.equals("cat")) {
            Cat cat = new Cat(age,name,breed,color,gender);
            try {
                animalShelter.put(++animalCount, cat);
            }
            catch (Exception e) {
                System.out.println("Приюта для кошек не существует");
            }
        }
        else if (animalDeposit.equals("dog")) {
            Dog dog = new Dog(age,name,breed,color,gender);
            try {
                animalShelter.put(++animalCount, dog);
            }
            catch (Exception e) {
                System.out.println("Приюта для собак не существует");
            }
        }
        else {
            try {
                throw new Exception();
            }
            catch (Exception e){
                System.out.println("Ахтунг! Мы пытаемся добавить в приют не кошку и не собаку.");
            }
        }
    }

    public static Object randomWithdraw(Map animalShelter) {
        Object withdrawAnimal;
        int shelterSize = 0;
        try {
            shelterSize = animalShelter.size();
        }
        catch (Exception e) {
            System.out.println("Приют определённо пуст");
        }
        Random random = new Random();
        Integer withdrawAnimalNumber = random.nextInt(shelterSize) + 1;
        withdrawAnimal = animalShelter.get(withdrawAnimalNumber);
        animalShelter.remove(withdrawAnimalNumber);
        return withdrawAnimal;
    }

    public static void choose(Map animalShelter) {
        System.out.println("Сорян, данный метод пока ещё не реализован. Сложна.");
    }
}
