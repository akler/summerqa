package lessonAuto7.Solution1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
1. Создать приют для бездомных кошек и собак. Должна быть реализована возможность добавлять животных в приют а также
возможность взять из приюта случайное животное;

2*. Добавить возможность забирать из приюта животное с
определёнными свойствами («Мама, хочу рыжую собачку!»).
 */
public class Solution5_1 {
    public static void main(String[] args) {

        Map<Integer, Animal> animalShelter = new HashMap<>();
        int option;

//      Наполняем приют разными тварями чтобы он не был пуст
        Cat cat = new Cat(3, "Мурлыка", "Персидская","черепаховый","девочка");
        Integer animalCount = AnimalShelter.getAnimalCount();
        animalShelter.put(++animalCount, cat);
        AnimalShelter.setAnimalCount(animalCount);

        Dog dog = new Dog(22,"Коржик","Шпиц","серый","мальчик");
        animalCount = AnimalShelter.getAnimalCount();
        animalShelter.put(++animalCount, dog);
        AnimalShelter.setAnimalCount(animalCount);

        Dog bigDog = new Dog(100500,"Rex","Бульдог","чёрный","мальчик");
        animalCount = AnimalShelter.getAnimalCount();
        animalShelter.put(++animalCount, bigDog);
        AnimalShelter.setAnimalCount(animalCount);

//      Меню
        System.out.println("Добро пожаловать в приют для животных.");
        do {
            System.out.println("");
            System.out.println("Введите номер пункта меню, выбрав одну из предложенных возможностей нашего приюта:");
            System.out.println("1: Сдать питомца в приют");
            System.out.println("2: Забрать любое животное из приюта");
            System.out.println("3: Забрать животное, подобрав по критериям");
            System.out.println("4: Уйти");
            Scanner scanner = new Scanner(System.in);
            try {
                option = scanner.nextInt();
            }
            catch (Exception e) {
                System.out.println("Вы ввели не число. Работа программы была остановлена.");
                option = 4;
            }

                switch(option) {
                    case 1:
        //              Сдаём животинку в приют
                        AnimalShelter.deposit(animalShelter);
        //              Смотрим, что животинка попала в приют
                        System.out.println("Сейчас в приюте " + animalShelter);
                        break;
                    case 2:
        //              Забираем рандомную животинку из приюта
                        Object withdrawAnimal = AnimalShelter.randomWithdraw(animalShelter);
                        System.out.println("Добрые люди забрали зверушку " + withdrawAnimal);
        //              Смотрим, что животинки больше нет в приюте
                        System.out.println("В приюте остались: " + animalShelter);
                        break;
                    case 3:
                        AnimalShelter.choose(animalShelter);
                        break;
                    case 4:
                        break;
                    default:
                        System.out.println("Такого пункта нет в меню. Давайте попробуем ещё раз");
                        break;
                }
        } while (option != 4);
    }
}
