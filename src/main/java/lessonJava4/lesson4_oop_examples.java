package lessonJava4;

public class lesson4_oop_examples {
    public static void main(String[] args){
        Dog dog = new Dog();
        Dog sharik = new Dog();
        dog.run();
        Cat cat = new Cat();
        cat.run();
        //Animal animal = new Animal();

        Duck duck = new Duck();
        duck.fly();
        Rocket rocket = new Rocket();
        rocket.fly();

        Flyable[] flyables = new Flyable[2];
        flyables[0] = duck;
        flyables[1] = rocket;
        }
}
