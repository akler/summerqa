package lessonJava4;

abstract class Animal {

    String name;
    int age;
    String breed;

    public void eat(){
        System.out.println("Я ем");
    }

    abstract void run();
}
