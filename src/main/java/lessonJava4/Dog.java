package lessonJava4;

public class Dog extends Animal {

    static int counter = 10;

    Dog(){
        System.out.println(Dog.counter);
        counter--;
    }

    @Override
    public void run(){
        System.out.println("Я собака, я бегу");
    }

}
