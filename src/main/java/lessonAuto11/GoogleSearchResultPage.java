package lessonAuto11;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
//import org.junit.jupiter.api.Assertation;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class GoogleSearchResultPage {

    public GoogleSearchResultPage resultsQuantityShouldBe(int size) {
        $$(".rc .r").shouldHaveSize(size);
        return this;
    }

    public GoogleSearchResultPage resultsTextShouldContain(String text) {
//        $(".rc .r").shouldHave(Condition.text(text));
        List<String> results = $$(".rc .r").texts();
//        Assertation.assertTrue(results
//        .stream()
//        .allMatch(element -> element.toLowerCase().contains(text.toLowerCase())));
        return this;
    }
}
