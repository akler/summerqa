package lessonAuto11;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class GoogleMainPage {

    public void openPage() {
        open("/");
    }

    public GoogleSearchResultPage searchForText(String text) {
        $("input#lst-ib")
                .shouldBe(Condition.visible)
                .setValue(text).pressEnter();
        return new GoogleSearchResultPage();
    }
}
