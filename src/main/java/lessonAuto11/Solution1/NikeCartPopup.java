package lessonAuto11.Solution1;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.WebElement;
import static com.codeborne.selenide.Selenide.$;

public class NikeCartPopup {

    //  Локатор POPUP добавлен отдельной константой, поскольку относительный путь к содержимому поп-апа ведёт всегда через него
    final static String POPUP = "div[class*=\"js-modal fx-modal modal\"]";
    final static String MODEL = POPUP + " " + "div > p[data-test='atc-confirmation-product-title']";
    final static String PRICE = POPUP + " " + "div[data-test='product-price']";
    final static String SELECTED_SIZE = POPUP + " " + "div > p[data-test='atc-confirmation-product-size']";
    final static String CART_VIEW_BUTTON = POPUP + " " + "button[data-test=\"qa-cart-view\"]";

    public WebElement findModel() {
        return $(MODEL);
    }

    public WebElement findPrice() {
        return $(PRICE);
    }

    public String selectedSize() {
        return $(SELECTED_SIZE).getText();
    }

    public void goToCart() {
        $(CART_VIEW_BUTTON).shouldBe(Condition.visible).click();
    }
}