package lessonAuto11.Solution1;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.Random;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class NikeStorePage {

    final static String PRODUCTS_LIST = "(//div[@class='grid-item-content'])";
    final static String MODEL = "//div[@class='product-name ']/p[1]";
    final static String PRICE = "//div[@class='prices']";

    public void open() {
        Selenide.open("/ru/ru_ru/");
    }

    public void chooseProductCategory(String type, String product) {
        $(By.xpath(type)).click();
        $(product).click();
    }

    public WebElement randomProductChoose() {
        Random random = new Random();
        int randomNumber = random.nextInt($$(By.xpath(PRODUCTS_LIST)).size() + 1);
        String index = "[" + randomNumber + "]";
        return $(By.xpath(PRODUCTS_LIST + index));
    }

    public WebElement findModel() {
        return $(By.xpath(MODEL));
    }

    public WebElement findPrice() {
        return $(By.xpath(PRICE));
    }
}