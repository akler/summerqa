package lessonAuto11.Solution1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;

public class NikeCartPage {

    final static String PRICE = "//div/p[@class='ch4_cartItemPrice']";
    final static String MODEL = "//div/a/p[@class='ch4_cartItemTitle']";

    public WebElement findModel() {
        return $(By.xpath(MODEL));
    }

    public WebElement findPrice() {
        return $(By.xpath(PRICE));
    }
}