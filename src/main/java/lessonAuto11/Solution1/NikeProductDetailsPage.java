package lessonAuto11.Solution1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static com.codeborne.selenide.Selenide.$;

public class NikeProductDetailsPage {

    final static String MODEL = "//div/h1[@data-test='product-title']";
    final static String PRICE = "//div[@data-test='product-price']";
    final static String AVAILABLE_SIZE = "div[name='skuAndSize'] > label";
    final static String ADD_TO_CART = "button.addToCartBtn";

    public WebElement findModel() {
        return $(By.xpath(MODEL));
    }

    public WebElement findPrice() {
        return $(By.xpath(PRICE));
    }

    // Первый доступный для товара размер
    public String selectSize() {
        WebElement size = $(AVAILABLE_SIZE);
        size.click();
        return size.getText();
    }

    public void addToCart() {
        $(ADD_TO_CART).click();
    }
}