package lessonAuto12.Solution1.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import ru.alfabank.alfatest.cucumber.annotations.Name;
import ru.alfabank.alfatest.cucumber.api.AkitaPage;

import java.util.List;

@Name("Каталог товаров")
public class NikeCatalogPage extends AkitaPage {

    @FindBy(xpath = "//div[@class='grid-item-content']")
    @Name("Список товаров")
    public List<SelenideElement> listGoods;

//    public List<SelenideElement> getListGoods() {
//        return listGoods;
//    }

    @FindBy(xpath = "//div[@class='product-name ']/p[1]")
    @Name("Модель из списка товаров")
    public SelenideElement modelFromStorePage;

    @FindBy(xpath = "//div[@class='prices']")
    @Name("Цена из списка товаров")
    public SelenideElement priceFromStorePage;

}
