package lessonAuto12.Solution1.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import ru.alfabank.alfatest.cucumber.annotations.Name;
import ru.alfabank.alfatest.cucumber.api.AkitaPage;

@Name("Детальная страница товара")
public class NikeDetailsPage extends AkitaPage {

    @FindBy(xpath = "//div/h1[@data-test=\"product-title\"]")
    @Name("Модель из детальной страницы")
    public SelenideElement modelFromDetailsPage;

    @FindBy(xpath = "//div[@data-test=\"product-price\"]")
    @Name("Цена из детальной страницы")
    public SelenideElement priceFromDetailsPage;

}
