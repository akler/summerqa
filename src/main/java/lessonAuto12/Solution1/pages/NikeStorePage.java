package lessonAuto12.Solution1.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import ru.alfabank.alfatest.cucumber.annotations.Name;
import ru.alfabank.alfatest.cucumber.annotations.Optional;
import ru.alfabank.alfatest.cucumber.api.AkitaPage;

@Name("Страница магазина Nike")
public class NikeStorePage extends AkitaPage {

    @FindBy(xpath = "//a/span[contains(text(),'МУЖЧИНЫ')]")
    @Name("Категория товаров")
    public SelenideElement goodsCategory;

    @Optional
    @FindBy(xpath = "//a[@data-subnav-label='Вся обувь']")
    @Name("Подкатегория товаров")
    public SelenideElement goodsSubategory;

}
