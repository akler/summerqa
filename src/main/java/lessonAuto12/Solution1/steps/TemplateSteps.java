package lessonAuto12.Solution1.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Пусть;
import cucumber.api.java.ru.Тогда;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.interactions.Actions;
import ru.alfabank.alfatest.cucumber.api.AkitaScenario;

import java.util.List;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

@Slf4j
public class TemplateSteps {

    private AkitaScenario akitaScenario = AkitaScenario.getInstance();

    private int getRandom(int maxValueInRange) {
        return (int) (Math.random() * maxValueInRange);
    }

    /**
     */
    @Пусть("курсор наведён на элемент \"([^\"]*)\"$")
    public void cursorHoverOnElement(String elementName) {
        SelenideElement element = akitaScenario.getCurrentPage().getElement(elementName);
        Actions builder = new Actions(getWebDriver());
        builder.moveToElement(element).click().build().perform();
    }

    /**
     * Выбор из списка со страницы любого случайного элемента и сохранение его значения в переменную
     */
    @Когда("^перед кликом по элементу из списка \"([^\"]*)\" текст элемента сохранён в переменную \"([^\"]*)\"$")
    public void beforeClickOnElementSaveTextElementIntoVar(String listName, String varName) {
        List<SelenideElement> listOfElementsFromPage = akitaScenario.getCurrentPage().getElementsList(listName);
        SelenideElement element = listOfElementsFromPage.get(getRandom(listOfElementsFromPage.size()));
        akitaScenario.setVar(varName, akitaScenario.getCurrentPage().getAnyElementText(element).trim());
        akitaScenario.write(String.format("Переменной [%s] присвоено значение [%s] из списка [%s]", varName,
                akitaScenario.getVar(varName), listName));
        element.shouldBe(Condition.visible).click();
    }

    @Тогда("^значение (?:поля|элемента) \"([^\"]*)\" содержится в значении из переменной \"([^\"]*)\"$")
    public void checkElementTextContainsInVar(String elementName, String variableName) {
        String actualValue = akitaScenario.getCurrentPage().getAnyElementText(elementName).trim();
        String expectedValue = akitaScenario.getVar(variableName).toString();
        assert expectedValue.contains(actualValue);
    }


}